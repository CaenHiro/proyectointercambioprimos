﻿create table Primos (
idPrimo integer primary key,
nombre varchar(45),
correo varchar(45),
fechaNacimiento date,
idHermano integer
);

create  table Hermanos(
idHermano integer primary key, 
idHermanoP integer references Primos(idPrimo),
idHermanoS integer references Primos(idPrimo)
);

insert into primos values (1, 'Laura Adriana Verde Escalona','lauraverde89@gmail.com' , '09/08/1989' ,3);

insert into primos values (2, 'Carlos Augusto Escalona Navarro','caen@ciencias.unam.mx' , '24/02/1992',6);

insert into primos values (3, 'Ana Karen Verde Escalona','anaverde94@hotmail.com' , '16/11/1994' ,1);

insert into primos values (4, 'Aide Escalona Escobar',' ' , '05/01/1995',7);

insert into primos values (5, 'Daniela Montserrat Barredo Escalona','anaverde94@hotmail.com' , '27/04/1995',8);

insert into primos values (6, 'Karla Escalona Navarro','es_na_k@hotmail.comm' , '31/08/1995',2);

insert into primos values (7, 'Sheila Ivonne Escalona Escobar','  ' , '30/08/1997',4);

insert into primos values (8, 'Paola Betsabeth Barredo Escalona','kiss15adict@gmail.com' , '06/09/1997',5);

insert into primos values (9, 'Aldo Elias __ Escalona','  ' , '  ',0);

insert into hermanos values (1,1,3);
insert into hermanos values (2,2,6);
insert into hermanos values (3,4,7);









