package primos;

/**
 *
 * @author CAEN
 */
public class TuplaPrimos {

    private Primo primoDa;
    private Primo primoRecibe;

    /**
     *
     * @param a
     * @param b
     */
    public TuplaPrimos(Primo a, Primo b) {
        this.primoDa = a;
        this.primoRecibe = b;
    }

    /**
     * @return the primoDa
     */
    public Primo getPrimoDa() {
        return primoDa;
    }

    /**
     * @param primoDa the primoDa to set
     */
    public void setPrimoDa(Primo primoDa) {
        this.primoDa = primoDa;
    }

    /**
     * @return the primoRecibe
     */
    public Primo getPrimoRecibe() {
        return primoRecibe;
    }

    /**
     * @param primoRecibe the primoRecibe to set
     */
    public void setPrimoRecibe(Primo primoRecibe) {
        this.primoRecibe = primoRecibe;
    }

    public String toString() {
        return "A : " + this.primoDa.toString() + " le tocadar a : " + this.primoRecibe.toString();
    }

}
