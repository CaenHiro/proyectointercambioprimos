package primos;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author CAEN
 */
public class OperacionesPrimos {

    ArrayList<Primo> listaPrimos = new ArrayList<Primo>();
    ArrayList<Primo> listaPrimos1 = new ArrayList<Primo>();
    ArrayList<Primo> listaPrimos2 = new ArrayList<Primo>();
    ArrayList<TuplaPrimos> listaFinalPrimos = new ArrayList<TuplaPrimos>();

    /**
     *
     */
    public OperacionesPrimos() {
        this.listaPrimos.add(new Primo("Karla", "es_na_k@hotmail.com", new Primo("Yael", "S/C"), new Primo("Carlos")));
        this.listaPrimos.add(new Primo("Laura", "lauraverde89@gmail.com", new Primo("Tania", "S/C"), new Primo("Ana")));
        this.listaPrimos.add(new Primo("Daniela", "dan_yaah@hotmail.com", new Primo("Ivonne", "ivonne_escalona@outlook.com"), new Primo("Paola")));
        this.listaPrimos.add(new Primo("Paola", "kiss15adict@gmail.com", new Primo("Karla", "es_na_k@hotmail.com"), new Primo("Daniela")));
        this.listaPrimos.add(new Primo("Ivonne", "ivonne_escalona@outlook.com", new Primo("Paola", "kiss15adict@gmail.com"), new Primo("Ayde")));
        this.listaPrimos.add(new Primo("Ayde", "lizzi_escalona@hotmail.com", new Primo("Daniela", "dan_yaah@hotmail.com"), new Primo("Ivonne")));
        this.listaPrimos.add(new Primo("Carlos", "caen@ciencias.unam.mx", new Primo("Laura", "lauraverde89@gmail.com"), new Primo("Karla")));
        this.listaPrimos.add(new Primo("Aldo", "S/C", new Primo("Carlos", "caen@ciencias.unam.mx")));
        this.listaPrimos.add(new Primo("Yael", "S/C", new Primo("Ayde", "lizzi_escalona@hotmail.com"), new Primo("Tania")));
        this.listaPrimos.add(new Primo("Tania", "S/C", new Primo("Aldo", "S/C"), new Primo("Yael")));
        this.listaPrimos.add(new Primo("Ana", "S/C", null, new Primo("Yael")));
        this.listaPrimos1.addAll(listaPrimos);
        this.listaPrimos2.addAll(listaPrimos);
    }

    /**
     *
     * @return
     */
    public ArrayList<TuplaPrimos> asignaRegaloPrimo() {
        Random rnd = new Random();
        int cont = 0;
        int contTotal = this.listaPrimos1.size();
        while (cont < contTotal) {
            Primo tmpP1 = this.listaPrimos.get(cont);
            this.listaPrimos2.remove(tmpP1);
            int indicePrimo2 = (int) (rnd.nextDouble() * this.listaPrimos2.size());
            Primo tmpRecibe = listaPrimos2.get(indicePrimo2);
            if (tmpP1.getRegaloAnioPasado() != null) {
                if (!tmpP1.getRegaloAnioPasado().getNombre().equals(tmpRecibe.getNombre())) {
                    if (!tmpP1.esHermoDe(tmpRecibe)) {
                        TuplaPrimos tmpF = new TuplaPrimos(tmpP1, tmpRecibe);
                        this.listaFinalPrimos.add(tmpF);
                        System.out.println(tmpF.toString());
                        this.listaPrimos1.remove(tmpP1);
                        this.listaPrimos2.add(tmpP1);
                        this.listaPrimos2.remove(tmpRecibe);
                        cont++;
                    }
                }
            }else{
                if (!tmpP1.esHermoDe(tmpRecibe)) {
                    TuplaPrimos tmpF = new TuplaPrimos(tmpP1, tmpRecibe);
                    this.listaFinalPrimos.add(tmpF);
                    System.out.println(tmpF.toString());
                    this.listaPrimos1.remove(tmpP1);
                    this.listaPrimos2.add(tmpP1);
                    this.listaPrimos2.remove(tmpRecibe);
                    cont++;
                }
            }
        }
        return listaFinalPrimos;
    }

}
