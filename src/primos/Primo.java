package primos;

/**
 *
 * @author CAEN
 */
public class Primo {

    private String nombre;
    private String correo;
    private Primo regaloAnioPasado;
    private Primo hermono;

    /**
     *
     * @param nombre
     */
    public Primo(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @param nombre
     * @param mail
     */
    public Primo(String nombre, String mail) {
        this.nombre = nombre;
        this.correo = mail;
    }

    /**
     *
     * @param nombre
     * @param mail
     * @param dioAnioPasado
     */
    public Primo(String nombre, String mail, Primo dioAnioPasado) {
        this.nombre = nombre;
        this.correo = mail;
        this.regaloAnioPasado = dioAnioPasado;
    }

    /**
     *
     * @param nombre
     * @param mail
     * @param dioAnioPasado
     * @param hermano
     */
    public Primo(String nombre, String mail, Primo dioAnioPasado, Primo hermano) {
        this.nombre = nombre;
        this.correo = mail;
        this.regaloAnioPasado = dioAnioPasado;
        this.hermono = hermano;
    }

    /**
     *
     * @param p1
     */
    public Primo(Primo p1) {
        this.nombre = p1.nombre;
        this.correo = p1.correo;
        this.regaloAnioPasado = p1.regaloAnioPasado;
        this.hermono = p1.hermono;
    }

    /**
     *
     * @param p1
     * @return
     */
    public boolean esHermoDe(Primo p1) {
        if (p1.hermono != null && this.hermono != null) {
            if (this.hermono.getNombre().equals(p1.getNombre())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String toString() {
        return this.getNombre();
    }

    /**
     * @return the regaloAnioPasado
     */
    public Primo getRegaloAnioPasado() {
        return regaloAnioPasado;
    }

    /**
     * @param regaloAnioPasado the regaloAnioPasado to set
     */
    public void setRegaloAnioPasado(Primo regaloAnioPasado) {
        this.regaloAnioPasado = regaloAnioPasado;
    }

    /**
     * @return the hermono
     */
    public Primo getHermono() {
        return hermono;
    }

    /**
     * @param hermono the hermono to set
     */
    public void setHermono(Primo hermono) {
        this.hermono = hermono;
    }

}
